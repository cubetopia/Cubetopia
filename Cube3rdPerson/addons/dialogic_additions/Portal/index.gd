@tool
extends DialogicIndexer

func _get_events() -> Array:
	return [this_folder.path_join('event_portal.gd')]

func _get_subsystems() -> Array[Dictionary]:
	return [{"name": "Portals", "script": this_folder.path_join('subsystem_portals.gd')}]
