@tool
extends DialogicIndexer

func _get_events() -> Array:
	return [this_folder.path_join('event_sky_text.gd')]

func _get_subsystems() -> Array[Dictionary]:
	return [{"name": "SkyText", "script": this_folder.path_join('subsystem_sky_text.gd')}]
