@tool
extends DialogicEvent
class_name DialogicSkyTextEvent

# Define properties of the event here

var text : String

func _execute() -> void:
	print("SETTING SKY TEXT TO ", text)
	Dialogic.SkyText.set_text(text)
	
	finish() # called to continue with the next event


#region INITIALIZE
################################################################################
# Set fixed settings of this event
func _init() -> void:
	event_name = "Sky Text"
	event_category = "Other"



#endregion

#region SAVING/LOADING
################################################################################
func get_shortcode() -> String:
	return "sky_text"

func get_shortcode_parameters() -> Dictionary:
	return {
		"text"		: {"property": "text", "default": ""},
	}

# You can alternatively overwrite these 3 functions: to_text(), from_text(), is_valid_event()
#endregion


#region EDITOR REPRESENTATION
################################################################################

func build_event_editor() -> void:
	add_header_edit("text", DialogicEvent.ValueType.SINGLELINE_TEXT, {'left_text': "Text"})
	pass

#endregion
