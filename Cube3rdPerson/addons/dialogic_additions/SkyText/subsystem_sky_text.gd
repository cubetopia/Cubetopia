extends DialogicSubsystem

## Describe the subsystems purpose here.


#region STATE
####################################################################################################

func clear_game_state(clear_flag:=Dialogic.ClearFlags.FULL_CLEAR) -> void:
	pass

func load_game_state(load_flag:=LoadFlags.FULL_LOAD) -> void:
	pass

#endregion


#region MAIN METHODS
####################################################################################################

func set_text(text: String) -> void:
	for node in get_tree().get_nodes_in_group("dialogic_custom_nodes"):
		if node.name == "SkyTextLabel":
			node.text = text

# Add some useful methods here.

#endregion
