@tool
extends DialogicIndexer

func _get_subsystems() -> Array:
	return [{'name':'BallMover', 'script':this_folder.path_join('subsystem_ball_mover.gd')}]
