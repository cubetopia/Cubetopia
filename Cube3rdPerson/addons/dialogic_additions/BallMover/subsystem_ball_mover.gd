extends DialogicSubsystem

## Describe the subsystems purpose here.


#region STATE
####################################################################################################

func clear_game_state(clear_flag:=Dialogic.ClearFlags.FULL_CLEAR) -> void:
	pass

func load_game_state(load_flag:=LoadFlags.FULL_LOAD) -> void:
	pass

#endregion


#region MAIN METHODS

func _ready():
	Dialogic.VAR.variable_changed.connect(_on_dialogic_var_changed)
	
func _on_dialogic_var_changed(info: Dictionary) -> void:
	if info.variable.begins_with("Map"):
		var x = Dialogic.VAR.Map.Globality
		var y = Dialogic.VAR.Map.Tech
		var z = Dialogic.VAR.Map.Economy
		move_ball(Vector3i(x, y, z))
	

func move_ball(position: Vector3i) -> void:
	for node in get_tree().get_nodes_in_group("dialogic_custom_nodes"):
		if node.name == "Ball":
			node.position = position
			print(node.transform)

####################################################################################################

# Add some useful methods here.

#endregion
