# CUBETOPIA

#### Submitted to CCC Call for Stories, February 2024


## The project

Cubetopia is a narrative Free software video
game and interactive art experience where the player is
asked to build their own perfect future, one narrative
decision at a time.

With each step come societal, environmental and political
choices and impacts. There are no right or wrong answers,
they each present their own challenges, which provides the
game with replayability.

Players do have a trick up their sleeves: when time feels
right, they can hack through their choices a limited number
of times for colossal changes, getting the best of both
worlds or branching off to much stranger visions of the
future.

As the choices are made, the visual art and music become
more complex and reflect the player’s choices. At the end,
the player will be able to download a complete song and
tableau as a keepsake of their personal utopia.

```
“Try to look at what the world was like a few hundred years ago,
and try to imagine what you want the world to look like in a
hundred years. What do you want it to look like? What do you
think is right for people in a hundred years? How do you hope
people that you will never meet will live?” 
- No Neutral Ground in a Burning World, 30C
```



### Navigating through choices

## The cube

The choices presented to players implicitly draw on utopian
philosophies, both those with centuries of thought behind
them and younger movements, and, explicitly, their presence
in the CCC community.

The basic bones of the game are arranged on a
three-dimensional decision cube, where the emphasis goes
from central planning to commerce, small communities to
global cooperation, and from nature to high-tech and
space-faring. These are the primary metrics.

Special mechanics will sometimes allow the player to get
the best of both worlds, or branch off from the cube to
stranger outcomes.

The challenge of the game is to guarantee freedom, equity
and accountability in each type of society.



## Narrative beats

The underlying mechanic of the game prompts the player to
choose between the different types of utopian societies,
with a follow-up narrative exploring the unique challenges of
each, and the player’s choices here adjusting the overall
position on the cube.

Some of these decisions may influence secondary metrics,
such as personal autonomy, transhumanism or
sustainability. These are tracked implicitly and influence the
outcome.


```
“The hacker mindset is a post-apocalyptically appropriate
way of thinking. Wouldn’t you run to your hackerspace, if
things really went wrong? If the world went down in chaos,
I would really want it to happen somewhere at the end of
December, because you all would be the people I would
want to be around.” 
- 10 Years After “We Lost the War”, 32C
```


## Hacking the future

Sometimes the perfect solution to a problem does not exist yet. We need to invent it. Sometimes we are determined to get the best of both worlds, or want to branch out in an unexplored direction.

There will be a small resource in the game of additional, unexpected solutions. These might be as yet nonexistent technology, biopunk speculation or branching out far afield with a leap of faith. These will have profound effect of the final shape of the utopia, and the accompanying art. The game will also - and this should go without saying - be Free software. And so everyone is welcome to modify it and add their own visions and ideas.


```
A video with a music demo is available at
https://brettpreston.github.io/audio/Brett_Preston_-_Cube_Audio_Demo(720p).mp
```
## Impact through music

As the player’s vision of the future shapes up as either organic or synthetic, chaotic or planned and epic or intimate, so does the music. The music will be pre-composed, and it will offer a variety of arrangements. The theme will be uplifting and optimistic, as demonstrated in the video sample.

Even the roads less travelled, and choices that are a little more out there will be represented in the music with smaller elements that correspond to the themes. After finishing the game, the customized piece of music will be available for download.


## (Mostly) FOSS tech stack

We are determined to use Free software in our game development stack whenever possible, and we are excited to take full advantage of its capabilities.

We are hoping to build Cubetopia in the Godot engine, with the help of other FOSS tools, such as Blender and Krita.

There are very few areas where proprietary tools are hard to avoid, but one of them is audio middleware. With a musical experience that requires complicated logic, it wouldn’t be realistic to commit to not take advantage of FMOD and its free (of cost) use for small projects.


## Accessibility considerations

This game places an emphasis on both the visual and audio experience, but they run in parallel. They can each be enjoyed without the other.

It has an easy binary choice mechanic which can be suited to many control interfaces.

A voice-over or alt-text option will be provided wherever necessary, and contrast factors will be verified.

It will be further evaluated for accessibility.

## CORE TEAM


```
Brett Preston is a music producer, sound designer, composer, and an audio director in VR gaming.

He is working on creating open-source hearing aids.

He is also a generative 3D artist, who creates psychedelic visuals and experiences.

https://zaga.bandcamp.com/

https://brettpreston.github.io/portfolio
```
```
Dr Eva Infeld is a mathematician, cypherpunk, a grassroots organiser, and brainstormer.

She went from working in academia to building privacy software, and has been a part of the CCC community for over a
decade. She’s the research lead on the Katzenpost anonymity project.

https://evainfeld.github.io/

```
```
Natalia Vish is a multimedia storyteller and graphic designer.
 
She works with scientists, engineers, journalists, and anybody else who has a story to tell but needs help finding their voice.
 
https://help-the-helper.nl/
 
https://nataliavish.com
```



## Scope and planning
```

Because of the budget constraints, we are ready to commit to a limited scope.

The scope we propose is a narrative choice game with 6 themes in art assets and music, combining to 8 standard outcomes, as illustrated on the decision cube.

12 additional narrative decisions, that affect art and music in a lesser way. In a typical playthrough a player sees a combination of 6 of them.


This will be enough to provide the player with a memorable and rewarding experience and it will be possible to easily expand on the project in the future, should additional support become available.


During the upcoming months we hope to devote time to game design and perhaps replace the basic card-based choice we implied in these slides with a different experience, which could be either art-focused, or mini-games that make players spend more time with some decisions. The card form is a simple one that we are comfortable committing to as an MVP.
```

## Budget breakdown

```
The budget allocation will be task-based. Contributions will be logged, with a lead in each section responsible for the
final decision over the split within the section. The budget percentages will be as follows:
```
```
Art 25%
```
```
Audio 20%
```
```
Coding 30%
```
```
Game design 10%
```
```
Writing 15%
```
```
Implementing audio logic and generative art will partially fall under coding, and project management is left out of the budget breakdown and volunteered as support.
```




